#!/bin/sh

check_var () {

    if [ -z "$USCRIPT" ]
    then
	echo "Entrer votre adresse mail epitech"

	read em

	if  [ -f ~/.bashrc ] && [ $SHELL = "/bin/sh" ]
	then
	    echo "\nexport USCRIPT=\"-u $em\"" >> ~/.bashrc
	    source ~/.bashrc
	elif [ -f ~/.zshrc ] && [ $SHELL = "/bin/zsh" ]
	then
	    echo "\nexport USCRIPT=\"-u $em\"" >> ~/.zshrc
	    source ~/.zshrc
	fi
    fi
    if [ -z "$ULOGIN" ]
    then
	echo "Entrer votre login (ex: marie-_j) epitech"

	read lv

	if  [ -f ~/.bashrc ] && [ $SHELL = "/bin/sh" ]
	then
	    echo "\nexport ULOGIN=\"$lv\"" >> ~/.bashrc
	    source ~/.bashrc
	elif [ -f ~/.zshrc ] && [ $SHELL = "/bin/zsh" ]
	then
	    echo "\nexport ULOGIN=\"$lv\"" >> ~/.zshrc
	    source ~/.zshrc
	fi
    fi


}

if [ -d ~/bin ]
then
    bfold=1
    echo "Le dossier bin existe"
    echo ""
    echo "Backup dossier"
    tar -cf backcup_bin.tar ./bin
    #    mv backcup_bin.tar ~/
    rm -fr ~/bin
else
    bfold=0
    echo "Dossier bin non existant"
    echo "Creation dossier bin"
    mkdir ~/bin
fi

git clone --depth 1 https://gitlab.com/theduskofdeath/Tek0.git ~/bin/ || {
    echo "Le clone a fail vérifier si git est installé"
}

if [ $bfold -eq 1 ]
then
    echo "Décompression backup dossier bin\n"

    echo "I"
    cd ~/
    tar -xf ~/backcup_bin.tar
    rm -fr ~/backcup_bin.tar
    cd $OLDPWD
fi

check_var

#sed '/\export PATH/a export USCRIPT="-u $em"' ~/.bashrc
